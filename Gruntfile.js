/**
 * Created by SASi on 10-Aug-16.
 */

module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        pug: {
            compile: {
                options: {
                    pretty: true
                },
                files: [{
                    cwd: 'src/pug',
                    src: '*.pug',
                    dest: 'build/',
                    expand: true,
                    ext: '.html'
                }]
            }
        },
        sass: {
            compile: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'build/css/styles.css': 'src/scss/styles.scss'
                }
            }
        },
        watch:{
            options: {
                livereload: {
                    host: 'localhost',
                    port: 9000
                }
            },
            pug:{
                files: ['src/pug/**/*.pug'],
                tasks: ['pug:compile']
            },
            sass:{
                files: ['src/scss/**/*.scss'],
                tasks: ['sass:compile']
            }
        }
    });


    grunt.loadNpmTasks('grunt-contrib-pug');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');


    grunt.registerTask('default', ['pug', 'sass', 'watch']);

};